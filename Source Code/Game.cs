﻿using System.Windows.Forms;

namespace TreasuresOrTroubles
{
    public class Game
    {
        public int perviousTurnScore = 0;
        public int currentTurnScore = 0;
        public int currentPlayerId = 0;
        public bool firstRoll = false;
        public bool inTheIslandOfSkull;
        public bool fullChest = false;
        public CardsPile cardsPile = new CardsPile();
        public Card currentCard;
        public Cubes logicalCubes = new Cubes();
        public Player[] players = new Player[Constants.NumOfPlayers];
        private readonly string[] m_botsNames = new string[Constants.NumOfBots] { "Michael", "Miriam", "Shmulik" };

        public Game()
        {
            players[0] = new Player();
            for (int i = 1; i < players.Length; i++)
            {
                players[i] = new Player(m_botsNames[i - 1]);
            }
        }

        public void NewCard()
        {
            currentCard = cardsPile.GetCard();
        }

        public static bool InSeaBattle(Card card)
        {
            return Constants.SeaBattles.ContainsKey(card);
        }

        public bool Roll()
        {
            logicalCubes.Roll();
            return GetTurnState();
        }

        public bool Roll(bool[] cubesToRoll)
        {
            logicalCubes.Roll(cubesToRoll);
            return GetTurnState();
        }

        private bool GetTurnState()
        {
            (bool isDead, int score) = Logic.CurrentTurnState(logicalCubes, ref currentCard, ref inTheIslandOfSkull, ref fullChest, firstRoll);
            perviousTurnScore = currentTurnScore;
            currentTurnScore = score;
            if (inTheIslandOfSkull && perviousTurnScore == currentTurnScore)
            {
                return true;
            }
            return isDead;
        }

        public bool NextTurn()
        {
            if (inTheIslandOfSkull)
            {
                for (int i = 0; i < players.Length; i++)
                {
                    if (i != currentPlayerId)
                    {
                        players[i].AddScore(currentTurnScore * -1);
                    }
                }
            }
            else
            {
                players[currentPlayerId].AddScore(currentTurnScore);
            }

            if (players[currentPlayerId].GetScore() >= Constants.ScoreToWin)
            {
                return true;
            }

            currentPlayerId = (currentPlayerId + 1) % Constants.NumOfPlayers;

            logicalCubes.ResetCubes();
            firstRoll = false;
            inTheIslandOfSkull = false;
            fullChest = false;
            perviousTurnScore = 0;
            currentTurnScore = 0;
            NewCard();
            return false;
        }

        public void EndGame()
        {
            string winnerName = "None";
            switch (currentPlayerId)
            {
                case 0:
                    winnerName = "You";
                    break;
                default:
                    winnerName = m_botsNames[currentPlayerId - 1];
                    break;
            }

            MessageBox.Show(winnerName + " won!", Properties.Resources.Game_Over, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
