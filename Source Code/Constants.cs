﻿using System.Collections.Generic;

namespace TreasuresOrTroubles
{
    class Constants
    {
        public const int SkullsListId = 0;
        public const int ToKeepListId = 1;
        public const int ToRollListId = 2;

        public const int ScoreToWin = 8000;
        public const int NumOfCubes = 8;
        public const int CubeSize = 60;
        public const int MinimumNumOfCubesToRoll = 2;
        public const int MinimumNumOfCubesToRollOnIOS = 1;
        public const int NumOfPlayers = 4;
        public const int NumOfBots = NumOfPlayers - 1;
        public const int SkullsToIOS = 4;
        public const int PointsPerSkullInIOS = 100;
        public const int NumOfSkullsToDead = 3;
        public const int PointsPerSwordInTruce = -1000;
        public const int PointsPerCoinAndDiamond = 100;
        public const int PointsForFullChest = 500;
        public static readonly List<CubeValue> CubeValues = new List<CubeValue>()
        {
            CubeValue.Skull,
            CubeValue.Parrot,
            CubeValue.Monkey,
            CubeValue.Sword,
            CubeValue.Coin,
            CubeValue.Diamond
        };
        public static readonly Dictionary<Card, (int amount, int value)> SeaBattles = new Dictionary<Card, (int, int)>()
        {
            { Card.SeaBattle2, (2, 300) },
            { Card.SeaBattle3, (3, 500) },
            { Card.SeaBattle4, (4, 1000) }
        };
        public static readonly Dictionary<Card, int> NumOfEachCardType = new Dictionary<Card, int>
        {
            { Card.Coin, 4 },
            { Card.Diamond, 4 },
            { Card.Captain, 4 },
            { Card.MonkeyBusiness, 4 },
            { Card.Skulls1, 3 },
            { Card.Skulls2, 2 },
            // { Card.SORCERESS, 4 },      For future development
            // { Card.STORM, 4 }          For future development
            { Card.SeaBattle2, 2 },
            { Card.SeaBattle3, 2 },
            { Card.SeaBattle4, 2 },
            { Card.TreasureChest, 4 },
            { Card.Truce, 4 }
        };
    }
}
