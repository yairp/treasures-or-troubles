﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace TreasuresOrTroubles
{
    public enum Card
    {
        Coin,
        Diamond,
        SeaBattle2,
        SeaBattle3,
        SeaBattle4,
        Captain,
        MonkeyBusiness,
        Skulls1,
        Skulls2,
        TreasureChest,
        Truce,
        Storm,        //   For future development
        Sorceress,    //   For future development
        UsedSorceress //   For future development
    }

    public class CardsPile
    {
        //fields
        private readonly Stack<Card> m_pile;
        private readonly int m_pileSize;
        private static readonly Random m_rnd = new Random();

        //constructor
        public CardsPile()
        {
            m_pile = new Stack<Card>();
            m_pileSize = 0;
            foreach (int amount in Constants.NumOfEachCardType.Values)
            {
                m_pileSize += amount;
            }
        }

        //functions
        private void ShuffleArr(Card[] cards)
        {
            Card temp;
            int r;
            for (int i = 0; i < cards.Length; i++)
            {
                temp = cards[i];
                r = m_rnd.Next(i, cards.Length);
                cards[i] = cards[r];
                cards[r] = temp;
            }
        }

        //methods
        private void GeneratePile()
        {
            int i = 0;
            Card[] cards = new Card[m_pileSize];
            foreach (Card currCard in Constants.NumOfEachCardType.Keys)
            {
                for (int j = 0; j < Constants.NumOfEachCardType[currCard]; j++)
                {
                    cards[i] = currCard;
                    i++;
                }
            }

            ShuffleArr(cards);

            m_pile.Clear();
            for (i = 0; i < cards.Length; i++)
            {
                m_pile.Push(cards[i]);
            }
        }

        public Card GetCard()
        {
            if (m_pile.Count == 0)
            {
                GeneratePile();
            }
            return m_pile.Pop();
        }

        public static Image GetCardImage(Card card)
        {
            switch (card)
            {
                case Card.Coin:
                    return Properties.Resources.coin_card;
                case Card.Diamond:
                    return Properties.Resources.diamond_card;
                case Card.SeaBattle2:
                    return Properties.Resources.seabattle2;
                case Card.SeaBattle3:
                    return Properties.Resources.seabattle3;
                case Card.SeaBattle4:
                    return Properties.Resources.seabattle4;
                case Card.Captain:
                    return Properties.Resources.captain_card;
                case Card.MonkeyBusiness:
                    return Properties.Resources.monkeyBusiness_card;
                case Card.Skulls1:
                    return Properties.Resources.skulls1_card;
                case Card.Skulls2:
                    return Properties.Resources.skulls2;
                case Card.Sorceress:
                    return Properties.Resources.sorceress_card; //   For future development
                case Card.UsedSorceress:
                    return Properties.Resources.sorceress_card; //   For future development
                case Card.TreasureChest:
                    return Properties.Resources.treasureChest_card;
                case Card.Storm:
                    return Properties.Resources.storm_card;     //   For future development
                case Card.Truce:
                    return Properties.Resources.truce_card;
                default:
                    return Properties.Resources.coin_card;
            }
        }
    }
}
