﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TreasuresOrTroubles
{
    public partial class Main_Form : Form
    {
        private readonly List<CubeImage> m_skulls = new List<CubeImage>();
        private readonly List<CubeImage> m_toKeep = new List<CubeImage>();
        private readonly List<CubeImage> m_toRoll = new List<CubeImage>();
        private readonly Dictionary<List<CubeImage>, Point> m_topLeftCorners;
        private Game m_game = new Game();

        public Main_Form()
        {
            InitializeComponent();

            m_topLeftCorners = new Dictionary<List<CubeImage>, Point>
            {
                [m_skulls] = new Point(0, 0), // relative to the panel!
                [m_toKeep] = new Point(0, 150),
                [m_toRoll] = new Point(0, 300)
            };

            for (int i = 0; i < Constants.NumOfCubes; i++)
            {
                var cubeimage = new CubeImage(Cubes_Panel, this);
                m_toKeep.Add(cubeimage);
                AddToCubeList(cubeimage, Constants.ToRollListId);
            }

            m_game.NewCard();
            Card_PictureBox.Image = CardsPile.GetCardImage(m_game.currentCard);
            FinishTurn_Button.Visible = false;

            Name1_Label.Text = m_game.players[0].GetName();
            Name2_Label.Text = m_game.players[1].GetName();
            Name3_Label.Text = m_game.players[2].GetName();
            Name4_Label.Text = m_game.players[3].GetName();
        }

        private void AddToCubeList(CubeImage cube, int section)
        {
            List<CubeImage> destList;
            switch (section)
            {
                case Constants.SkullsListId:
                    destList = m_skulls;
                    break;
                case Constants.ToKeepListId:
                    destList = m_toKeep;
                    break;
                case Constants.ToRollListId:
                    destList = m_toRoll;
                    break;
                default:
                    destList = m_toRoll;
                    break;
            }
            List<CubeImage> oldList = null;
            // if contained in the other two lists: remove from them
            foreach (List<CubeImage> list in new List<CubeImage>[] { m_skulls, m_toKeep, m_toRoll })
            {
                if (list.Contains(cube))
                {
                    oldList = list;
                    list.Remove(cube);
                    break;
                }
            }
            destList.Add(cube);
            SortCubesListByFace(destList);

            if (oldList == null)
                throw new Exception("problem dude");

            ReloadLists(oldList, destList);
        }

        private void SortCubesListByFace(List<CubeImage> list)
        {
            list.Sort((ci1, ci2) => ci1.GetFace() - ci2.GetFace());
        }

        public void CubeClicked(CubeImage cubeImage)
        {
            if (cubeImage.GetFace() == CubeValue.Empty || !Roll_Button.Visible)
            {
                return;
            }
            if (m_toKeep.Contains(cubeImage))
            {
                AddToCubeList(cubeImage, Constants.ToRollListId);
            }
            else if (m_toRoll.Contains(cubeImage))
            {
                if (m_game.inTheIslandOfSkull)
                {
                    MessageBox.Show(Properties.Resources.Cant_keep_cubes_while_island_death, Properties.Resources.Invalid_Action, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                AddToCubeList(cubeImage, Constants.ToKeepListId);
            }
        }

        private void ReloadLists(params List<CubeImage>[] lists)
        {
            foreach (List<CubeImage> list in lists)
            {
                foreach (CubeImage cubeImage in list)
                {
                    Point tlc = m_topLeftCorners[list];

                    int x = tlc.X, y = tlc.Y, i = list.IndexOf(cubeImage);
                    if (i < 4)
                    {
                        x += Constants.CubeSize * i;
                        y += 0;
                    }
                    else
                    {
                        x += Constants.CubeSize * (7 - i);
                        y += Constants.CubeSize;
                    }
                    cubeImage.Location = new Point(x, y);
                }
            }
        }

        private void Roll_Button_Click(object sender, EventArgs e)
        {
            Roll();
        }

        public void Roll()
        {
            if (m_toRoll.Count < Constants.MinimumNumOfCubesToRoll && !m_game.inTheIslandOfSkull)
            {
                MessageBox.Show(Properties.Resources.Cant_Roll_less_than_2_cubes, Properties.Resources.Invalid_Action, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (m_toRoll[0].GetFace() == CubeValue.Empty)
            {
                m_game.firstRoll = true;
                FinishTurn_Button.Visible = true;
            }
            else if (m_game.firstRoll)
            {
                m_game.firstRoll = false;
            }
            bool[] cubesToRoll = new bool[Constants.NumOfCubes];
            for (int i = 0; i < cubesToRoll.Length; i++)
            {
                cubesToRoll[i] = false;
            }
            for (int i = 0; i < m_toRoll.Count; i++)
            {
                cubesToRoll[m_toRoll[i].GetIndex()] = true;
            }

            bool isDead = m_game.Roll(cubesToRoll);

            for (int i = 0; i < m_toRoll.Count; i++)
            {
                m_toRoll[i].SetFace(m_game.logicalCubes[m_toRoll[i].GetIndex()]);
                if (m_game.logicalCubes[m_toRoll[i].GetIndex()] == CubeValue.Skull)
                {
                    AddToCubeList(m_toRoll[i], Constants.SkullsListId);
                    i--;
                }
            }
            SortCubesListByFace(m_toRoll);
            ReloadLists(m_toRoll);

            if (isDead && !m_game.inTheIslandOfSkull)
            {
                Roll_Button.Visible = false;
            }
            else if (m_game.inTheIslandOfSkull)
            {
                Score_Label.ForeColor = System.Drawing.Color.Red;
                if (!(m_game.currentTurnScore > m_game.perviousTurnScore))
                {
                    Roll_Button.Visible = false;
                    FinishTurn_Button.Visible = true;
                }
                else
                {
                    FinishTurn_Button.Visible = false;
                }
            }
            Score_Label.Text = m_game.currentTurnScore.ToString();
        }

        public void ResetTurn()
        {
            foreach (List<CubeImage> list in new List<CubeImage>[] { m_skulls, m_toKeep })
            {
                while (list.Count != 0)
                {
                    AddToCubeList(list[0], Constants.ToRollListId);
                }
            }
            for (int i = 0; i < Constants.NumOfCubes; i++)
            {
                m_toRoll[i].SetFace(CubeValue.Empty);
            }
            Score_Label.ForeColor = System.Drawing.SystemColors.ControlText;
            Card_PictureBox.Image = CardsPile.GetCardImage(m_game.currentCard);
            Score_Label.Text = "0";
        }

        private void FinishTurn_Button_Click(object sender, EventArgs e)
        {
            FinishTurn_Button.Visible = false;
            if (m_game.NextTurn())
            {
                EndGame();
            }
            ReloadScores();

            for (int i = 1; i < m_game.players.Length; i++)
            {
                AI.BotPlay(ref m_game);
                if (m_game.NextTurn())
                {
                    EndGame();
                }
                ReloadScores();
            }
            ResetTurn();
            Roll_Button.Visible = true;
        }

        private void ReloadScores()
        {
            Score1_Label.Text = m_game.players[0].GetScore().ToString();
            Score2_Label.Text = m_game.players[1].GetScore().ToString();
            Score3_Label.Text = m_game.players[2].GetScore().ToString();
            Score4_Label.Text = m_game.players[3].GetScore().ToString();
        }

        private void EndGame()
        {
            m_game.EndGame();
            Close();
        }
    }
}
