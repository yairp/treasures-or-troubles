﻿using System.Collections.Generic;

namespace TreasuresOrTroubles
{
    public class Logic
    {
        public static (bool isDead, int moneySoFar) CurrentTurnState(Cubes cubes, ref Card currentCard, ref bool inIslandOfSkull, ref bool fullChest, bool firstTurn)
        {
            //Calculate skulls
            int numOfSkulls = 0;
            for (int i = 0; i < Constants.NumOfCubes; i++)
            {
                if (cubes[i] == CubeValue.Skull)
                {
                    numOfSkulls++;
                }
            }
            if (currentCard == Card.Skulls1)
            {
                numOfSkulls++;
            }
            else if (currentCard == Card.Skulls2)
            {
                numOfSkulls += 2;
            }
            List<CubeValue> cubeValues = new List<CubeValue>() { CubeValue.Parrot, CubeValue.Monkey, CubeValue.Sword, CubeValue.Coin, CubeValue.Diamond };
            Dictionary<CubeValue, int> cubesAmount = new Dictionary<CubeValue, int>();
            foreach (CubeValue cubeValue in cubeValues)
            {
                cubesAmount[cubeValue] = 0;
            }
            for (int i = 0; i < Constants.NumOfCubes; i++)
            {
                if (cubes[i] != CubeValue.Skull)
                {
                    cubesAmount[cubes[i]]++;
                }
            }
            if (currentCard == Card.Coin)
            {
                cubesAmount[CubeValue.Coin]++;
            }
            else if (currentCard == Card.Diamond)
            {
                cubesAmount[CubeValue.Diamond]++;
            }
            //Check if going to the island of death
            if (firstTurn && numOfSkulls >= Constants.SkullsToIOS && !Constants.SeaBattles.ContainsKey(currentCard))
            {
                inIslandOfSkull = true;
            }
            if (inIslandOfSkull)
            {
                return (false, numOfSkulls * Constants.PointsPerSkullInIOS * (currentCard == Card.Captain ? 2 : 1));
            }
            if (currentCard == Card.Truce && cubesAmount[CubeValue.Sword] > 0)
            {
                return (numOfSkulls >= Constants.NumOfSkullsToDead, cubesAmount[CubeValue.Sword] * Constants.PointsPerSwordInTruce);
            }
            //check if dead
            int score = 0;
            if (Game.InSeaBattle(currentCard))
            {
                if (numOfSkulls >= Constants.NumOfSkullsToDead || (cubesAmount[CubeValue.Sword] < Constants.SeaBattles[currentCard].amount))
                {
                    return (numOfSkulls >= Constants.NumOfSkullsToDead, Constants.SeaBattles[currentCard].value * -1);
                }
                else
                {
                    score += Constants.SeaBattles[currentCard].value;
                }
            }
            if (numOfSkulls >= Constants.NumOfSkullsToDead && currentCard != Card.TreasureChest)
            {
                return (true, 0);
            }
            //Calculate Score
            score += (cubesAmount[CubeValue.Coin] + cubesAmount[CubeValue.Diamond]) * Constants.PointsPerCoinAndDiamond;
            //  STORM card
            //if (currentCard == Card.STORM)
            //{
            //    score *= 2;
            //}
            //  STORM card

            fullChest = !(numOfSkulls > 0);
            score += CalculateSequence(cubesAmount[CubeValue.Coin], ref fullChest, false);
            score += CalculateSequence(cubesAmount[CubeValue.Diamond], ref fullChest, false);
            //  STORM card
            //if (currentCard == Card.STORM)
            //{
            //    return (false, score);
            //}
            //  STORM card

            if (currentCard == Card.MonkeyBusiness)
            {
                score += CalculateSequence(cubesAmount[CubeValue.Monkey] + cubesAmount[CubeValue.Parrot], ref fullChest);
            }
            else
            {
                score += CalculateSequence(cubesAmount[CubeValue.Monkey], ref fullChest);
                score += CalculateSequence(cubesAmount[CubeValue.Parrot], ref fullChest);
            }
            score += CalculateSequence(cubesAmount[CubeValue.Sword], ref fullChest);


            if (fullChest)
            {
                score += Constants.PointsForFullChest;
            }

            if (currentCard == Card.Captain)
            {
                score *= 2;
            }
            else if (numOfSkulls >= Constants.NumOfSkullsToDead)
            {
                return (true, score);
            }

            return (false, score);
        }

        private static int CalculateSequence(int len, ref bool fullChest, bool toChange = true)
        {
            switch (len)
            {
                case 3:
                    return 100;
                case 4:
                    return 200;
                case 5:
                    return 500;
                case 6:
                    return 1000;
                case 7:
                    return 2000;
                case 8:
                    return 4000;
                case 9:
                    return 8000;
                default:
                    if (toChange && len > 0) fullChest = false;
                    return 0;
            }
        }
    }
}
