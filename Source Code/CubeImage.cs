﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace TreasuresOrTroubles
{
    public class CubeImage : PictureBox
    {
        // fields
        private readonly Main_Form m_mainForm; // to call when clicked
        private CubeValue m_face;
        private readonly int m_index;
        private static int m_nextIndex = 0;

        public void SetFace(CubeValue newFace)
        {
            m_face = newFace;
            // switch case to update this.Image to appropriate image from Properties.Resources...
            switch (newFace)
            {
                case CubeValue.Skull:
                    Image = Properties.Resources.skull_dice;
                    break;
                case CubeValue.Parrot:
                    Image = Properties.Resources.parrot_dice;
                    break;
                case CubeValue.Monkey:
                    Image = Properties.Resources.monkey_dice;
                    break;
                case CubeValue.Sword:
                    Image = Properties.Resources.sword_dice;
                    break;
                case CubeValue.Coin:
                    Image = Properties.Resources.coin_dice;
                    break;
                case CubeValue.Diamond:
                    Image = Properties.Resources.diamond_dice;
                    break;
                case CubeValue.Empty:
                    Image = Properties.Resources.qm_dice;
                    break;
            }
        }

        public CubeValue GetFace()
        {
            return m_face;
        }

        public int GetIndex()
        {
            return m_index;
        }

        // basic properties
        public CubeImage(Panel bgPanel, Main_Form mainform)
        {
            Size = new Size(Constants.CubeSize, Constants.CubeSize);
            SizeMode = PictureBoxSizeMode.StretchImage;
            mainform.Controls.Add(this); // add me to the list of things that are to be drawn on the form
            Parent = bgPanel;
            BackColor = Color.Transparent;
            SetFace(CubeValue.Empty);
            Click += new System.EventHandler(Clicked);
            m_mainForm = mainform;
            m_index = m_nextIndex;
            m_nextIndex++;
        }

        // when clicked, tell form
        private void Clicked(object sender, EventArgs e)
        {
            m_mainForm.CubeClicked(this);
        }

    }
}
