﻿using System.Collections.Generic;

namespace TreasuresOrTroubles
{
    public class AI
    {
        public static void BotPlay(ref Game game)
        {
            bool quit = false;
            bool dead;
            bool[] cubesToRoll = new bool[Constants.NumOfCubes];

            for (int i = 0; i < Constants.NumOfCubes; i++)
            {
                cubesToRoll[i] = true;
            }

            game.firstRoll = true;
            do
            {
                dead = game.Roll(cubesToRoll);
                if (!dead)
                {
                    (quit, cubesToRoll) = ChooseMove(game.logicalCubes, game.currentCard, game.inTheIslandOfSkull, game.fullChest);
                }
                game.firstRoll = false;
            } while (!dead && !quit);
        }

        public static (bool quit, bool[] cubesToReRoll) ChooseMove(Cubes currCubes, Card currCard, bool inIOS, bool fullChest)
        {
            List<CubeValue> cubeValues = new List<CubeValue>() { CubeValue.Skull, CubeValue.Parrot, CubeValue.Monkey, CubeValue.Sword, CubeValue.Coin, CubeValue.Diamond };
            Dictionary<CubeValue, int> cubesAmount = new Dictionary<CubeValue, int>();
            foreach (CubeValue cubeValue in cubeValues)
            {
                cubesAmount[cubeValue] = 0;
            }
            for (int i = 0; i < Constants.NumOfCubes; i++)
            {
                cubesAmount[currCubes[i]]++;
            }
            if (currCard == Card.Skulls1)
            {
                cubesAmount[CubeValue.Skull]++;
            }
            else if (currCard == Card.Skulls2)
            {
                cubesAmount[CubeValue.Skull] += 2;
            }
            else if (currCard == Card.Coin)
            {
                cubesAmount[CubeValue.Coin]++;
            }
            else if (currCard == Card.Diamond)
            {
                cubesAmount[CubeValue.Diamond]++;
            }

            if (inIOS)
            {
                if (CanRoll(cubesAmount, true))
                {
                    return (false, ChooseCubes(currCubes, cubesAmount));
                }
                return (true, new bool[8]);
            }

            if (cubesAmount[CubeValue.Skull] == 2)
            {
                return (true, new bool[Constants.NumOfCubes]);
            }

            if (Game.InSeaBattle(currCard))
            {
                if (cubesAmount[CubeValue.Sword] >= Constants.SeaBattles[currCard].amount)
                {
                    return (true, new bool[Constants.NumOfCubes]);
                }
                else
                {
                    cubesAmount[CubeValue.Sword] = 0;
                    return (false, ChooseCubes(currCubes, cubesAmount));
                }
            }

            if (fullChest)
            {
                return (true, new bool[Constants.NumOfCubes]);
            }

            cubesAmount[CubeValue.Coin] = 0;
            cubesAmount[CubeValue.Diamond] = 0;
            if (CanRoll(cubesAmount))
            {
                return (false, ChooseCubes(currCubes, cubesAmount));
            }

            return (true, new bool[Constants.NumOfCubes]);
        }

        public static bool[] ChooseCubes(Cubes currCubes, Dictionary<CubeValue, int> toRoll)
        {
            toRoll[CubeValue.Skull] = 0;
            bool[] cubesToRoll = new bool[Constants.NumOfCubes];
            int currCubeTypeAmount;
            foreach (CubeValue cubeValue in Constants.CubeValues)
            {
                currCubeTypeAmount = toRoll[cubeValue];
                for (int i = 0; i < Constants.NumOfCubes && currCubeTypeAmount > 0; i++)
                {
                    if (currCubes[i] == cubeValue)
                    {
                        cubesToRoll[i] = true;
                        currCubeTypeAmount--;
                    }
                }
            }
            return cubesToRoll;
        }

        public static bool CanRoll(Dictionary<CubeValue, int> cubesAmount, bool inIOS = false)
        {
            int sum = 0;
            foreach (CubeValue cubeValue in Constants.CubeValues)
            {
                sum += cubesAmount[cubeValue];
            }
            return sum >= Constants.MinimumNumOfCubesToRoll || (inIOS && sum >= Constants.MinimumNumOfCubesToRollOnIOS);
        }
    }
}
