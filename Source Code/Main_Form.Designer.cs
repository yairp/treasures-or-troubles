﻿namespace TreasuresOrTroubles
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.Roll_Button = new System.Windows.Forms.Button();
            this.FinishTurn_Button = new System.Windows.Forms.Button();
            this.Cubes_Panel = new System.Windows.Forms.Panel();
            this.Logo_PictureBox = new System.Windows.Forms.PictureBox();
            this.CurrentScore_Label = new System.Windows.Forms.Label();
            this.Score_Label = new System.Windows.Forms.Label();
            this.Card_PictureBox = new System.Windows.Forms.PictureBox();
            this.Name1_Label = new System.Windows.Forms.Label();
            this.Name2_Label = new System.Windows.Forms.Label();
            this.Name3_Label = new System.Windows.Forms.Label();
            this.Name4_Label = new System.Windows.Forms.Label();
            this.Score4_Label = new System.Windows.Forms.Label();
            this.Score3_Label = new System.Windows.Forms.Label();
            this.Score2_Label = new System.Windows.Forms.Label();
            this.Score1_Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Logo_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card_PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // Roll_Button
            // 
            this.Roll_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Roll_Button.Location = new System.Drawing.Point(455, 388);
            this.Roll_Button.Name = "Roll_Button";
            this.Roll_Button.Size = new System.Drawing.Size(116, 57);
            this.Roll_Button.TabIndex = 0;
            this.Roll_Button.Text = "Roll";
            this.Roll_Button.UseVisualStyleBackColor = true;
            this.Roll_Button.Click += new System.EventHandler(this.Roll_Button_Click);
            // 
            // FinishTurn_Button
            // 
            this.FinishTurn_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FinishTurn_Button.Location = new System.Drawing.Point(309, 388);
            this.FinishTurn_Button.Name = "FinishTurn_Button";
            this.FinishTurn_Button.Size = new System.Drawing.Size(140, 57);
            this.FinishTurn_Button.TabIndex = 1;
            this.FinishTurn_Button.Text = "Finish Turn";
            this.FinishTurn_Button.UseVisualStyleBackColor = true;
            this.FinishTurn_Button.Click += new System.EventHandler(this.FinishTurn_Button_Click);
            // 
            // Cubes_Panel
            // 
            this.Cubes_Panel.BackgroundImage = global::TreasuresOrTroubles.Properties.Resources.bg;
            this.Cubes_Panel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Cubes_Panel.Location = new System.Drawing.Point(577, 12);
            this.Cubes_Panel.Name = "Cubes_Panel";
            this.Cubes_Panel.Size = new System.Drawing.Size(240, 433);
            this.Cubes_Panel.TabIndex = 0;
            // 
            // Logo_PictureBox
            // 
            this.Logo_PictureBox.BackColor = System.Drawing.Color.MistyRose;
            this.Logo_PictureBox.BackgroundImage = global::TreasuresOrTroubles.Properties.Resources.Otzarot_logo;
            this.Logo_PictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Logo_PictureBox.Location = new System.Drawing.Point(104, 12);
            this.Logo_PictureBox.Name = "Logo_PictureBox";
            this.Logo_PictureBox.Size = new System.Drawing.Size(416, 83);
            this.Logo_PictureBox.TabIndex = 2;
            this.Logo_PictureBox.TabStop = false;
            // 
            // CurrentScore_Label
            // 
            this.CurrentScore_Label.AutoSize = true;
            this.CurrentScore_Label.BackColor = System.Drawing.Color.Pink;
            this.CurrentScore_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CurrentScore_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrentScore_Label.Location = new System.Drawing.Point(460, 119);
            this.CurrentScore_Label.Name = "CurrentScore_Label";
            this.CurrentScore_Label.Size = new System.Drawing.Size(113, 26);
            this.CurrentScore_Label.TabIndex = 3;
            this.CurrentScore_Label.Text = ":ניקוד נוכחי";
            // 
            // Score_Label
            // 
            this.Score_Label.AutoSize = true;
            this.Score_Label.BackColor = System.Drawing.SystemColors.Control;
            this.Score_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Score_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score_Label.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Score_Label.Location = new System.Drawing.Point(460, 155);
            this.Score_Label.Name = "Score_Label";
            this.Score_Label.Size = new System.Drawing.Size(23, 26);
            this.Score_Label.TabIndex = 4;
            this.Score_Label.Text = "0";
            // 
            // Card_PictureBox
            // 
            this.Card_PictureBox.BackColor = System.Drawing.Color.Transparent;
            this.Card_PictureBox.Location = new System.Drawing.Point(12, 287);
            this.Card_PictureBox.Name = "Card_PictureBox";
            this.Card_PictureBox.Size = new System.Drawing.Size(105, 158);
            this.Card_PictureBox.TabIndex = 5;
            this.Card_PictureBox.TabStop = false;
            // 
            // Name1_Label
            // 
            this.Name1_Label.AutoSize = true;
            this.Name1_Label.BackColor = System.Drawing.Color.Gold;
            this.Name1_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Name1_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Name1_Label.Location = new System.Drawing.Point(13, 103);
            this.Name1_Label.Name = "Name1_Label";
            this.Name1_Label.Size = new System.Drawing.Size(2, 22);
            this.Name1_Label.TabIndex = 6;
            // 
            // Name2_Label
            // 
            this.Name2_Label.AutoSize = true;
            this.Name2_Label.BackColor = System.Drawing.Color.LightCoral;
            this.Name2_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Name2_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Name2_Label.Location = new System.Drawing.Point(13, 168);
            this.Name2_Label.Name = "Name2_Label";
            this.Name2_Label.Size = new System.Drawing.Size(2, 22);
            this.Name2_Label.TabIndex = 7;
            // 
            // Name3_Label
            // 
            this.Name3_Label.AutoSize = true;
            this.Name3_Label.BackColor = System.Drawing.Color.Lime;
            this.Name3_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Name3_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Name3_Label.Location = new System.Drawing.Point(148, 103);
            this.Name3_Label.Name = "Name3_Label";
            this.Name3_Label.Size = new System.Drawing.Size(2, 22);
            this.Name3_Label.TabIndex = 8;
            // 
            // Name4_Label
            // 
            this.Name4_Label.AutoSize = true;
            this.Name4_Label.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Name4_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Name4_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Name4_Label.Location = new System.Drawing.Point(148, 168);
            this.Name4_Label.Name = "Name4_Label";
            this.Name4_Label.Size = new System.Drawing.Size(2, 22);
            this.Name4_Label.TabIndex = 9;
            // 
            // Score4_Label
            // 
            this.Score4_Label.AutoSize = true;
            this.Score4_Label.BackColor = System.Drawing.SystemColors.Control;
            this.Score4_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Score4_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score4_Label.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Score4_Label.Location = new System.Drawing.Point(148, 194);
            this.Score4_Label.Name = "Score4_Label";
            this.Score4_Label.Size = new System.Drawing.Size(23, 26);
            this.Score4_Label.TabIndex = 10;
            this.Score4_Label.Text = "0";
            // 
            // Score3_Label
            // 
            this.Score3_Label.AutoSize = true;
            this.Score3_Label.BackColor = System.Drawing.SystemColors.Control;
            this.Score3_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Score3_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score3_Label.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Score3_Label.Location = new System.Drawing.Point(148, 129);
            this.Score3_Label.Name = "Score3_Label";
            this.Score3_Label.Size = new System.Drawing.Size(23, 26);
            this.Score3_Label.TabIndex = 11;
            this.Score3_Label.Text = "0";
            // 
            // Score2_Label
            // 
            this.Score2_Label.AutoSize = true;
            this.Score2_Label.BackColor = System.Drawing.SystemColors.Control;
            this.Score2_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Score2_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score2_Label.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Score2_Label.Location = new System.Drawing.Point(13, 194);
            this.Score2_Label.Name = "Score2_Label";
            this.Score2_Label.Size = new System.Drawing.Size(23, 26);
            this.Score2_Label.TabIndex = 12;
            this.Score2_Label.Text = "0";
            // 
            // Score1_Label
            // 
            this.Score1_Label.AutoSize = true;
            this.Score1_Label.BackColor = System.Drawing.SystemColors.Control;
            this.Score1_Label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Score1_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Score1_Label.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Score1_Label.Location = new System.Drawing.Point(12, 129);
            this.Score1_Label.Name = "Score1_Label";
            this.Score1_Label.Size = new System.Drawing.Size(23, 26);
            this.Score1_Label.TabIndex = 13;
            this.Score1_Label.Text = "0";
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TreasuresOrTroubles.Properties.Resources.PirateShip_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(829, 457);
            this.Controls.Add(this.Score1_Label);
            this.Controls.Add(this.Score2_Label);
            this.Controls.Add(this.Score3_Label);
            this.Controls.Add(this.Score4_Label);
            this.Controls.Add(this.Name4_Label);
            this.Controls.Add(this.Name3_Label);
            this.Controls.Add(this.Name2_Label);
            this.Controls.Add(this.Name1_Label);
            this.Controls.Add(this.Card_PictureBox);
            this.Controls.Add(this.Score_Label);
            this.Controls.Add(this.CurrentScore_Label);
            this.Controls.Add(this.Logo_PictureBox);
            this.Controls.Add(this.FinishTurn_Button);
            this.Controls.Add(this.Roll_Button);
            this.Controls.Add(this.Cubes_Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main_Form";
            this.Text = "אוצרות או צרות";
            ((System.ComponentModel.ISupportInitialize)(this.Logo_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Card_PictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Cubes_Panel;
        private System.Windows.Forms.Button Roll_Button;
        private System.Windows.Forms.Button FinishTurn_Button;
        private System.Windows.Forms.PictureBox Logo_PictureBox;
        private System.Windows.Forms.Label CurrentScore_Label;
        private System.Windows.Forms.Label Score_Label;
        private System.Windows.Forms.PictureBox Card_PictureBox;
        private System.Windows.Forms.Label Name1_Label;
        private System.Windows.Forms.Label Name2_Label;
        private System.Windows.Forms.Label Name3_Label;
        private System.Windows.Forms.Label Name4_Label;
        private System.Windows.Forms.Label Score4_Label;
        private System.Windows.Forms.Label Score3_Label;
        private System.Windows.Forms.Label Score2_Label;
        private System.Windows.Forms.Label Score1_Label;
    }
}

