﻿using Microsoft.VisualBasic;

namespace TreasuresOrTroubles
{
    public class Player
    {
        private string m_name;
        private int m_score;

        public Player()
        {
            m_name = Interaction.InputBox("Please enter your name:", "New Game");
            if (m_name == "") m_name = "John Doe";
            m_score = 0;
        }

        public Player(string name)
        {
            m_name = name;
        }

        public string GetName()
        {
            return m_name;
        }

        public int GetScore()
        {
            return m_score;
        }

        public void AddScore(int score)
        {
            m_score += score;
        }
    }
}
