﻿using System;

namespace TreasuresOrTroubles
{
    public enum CubeValue
    {
        Skull,
        Parrot,
        Monkey,
        Sword,
        Coin,
        Diamond,
        Empty
    }

    public class Cubes
    {
        //fields
        private readonly CubeValue[] m_cubes;
        private static readonly Random m_rnd = new Random();

        //constructor
        public Cubes()
        {
            m_cubes = new CubeValue[Constants.NumOfCubes];
            for (int i = 0; i < m_cubes.Length; i++)
            {
                m_cubes[i] = CubeValue.Empty;
            }
        }

        //methods
        public void Roll()
        {
            for (int i = 0; i < m_cubes.Length; i++)
            {
                m_cubes[i] = RollCube();
            }
        }

        public void Roll(bool[] cubes)
        {
            for (int i = 0; i < cubes.Length; i++)
            {
                if (cubes[i])
                {
                    m_cubes[i] = RollCube();
                }
            }
        }

        public CubeValue this[int i]
        {
            get => m_cubes[i];
        }

        private CubeValue RollCube()
        {
            return (CubeValue)m_rnd.Next(6);
        }

        public void ResetCubes()
        {
            for (int i = 0; i < m_cubes.Length; i++)
            {
                m_cubes[i] = CubeValue.Empty;
            }
        }
    }
}
